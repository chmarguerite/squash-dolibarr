*** Settings ***
Library           SeleniumLibrary
Library           String
Library           OperatingSystem

*** Variables ***
${urlDolibarr}    http://dolibarr.testlogiciel.io/
&{IDENTIFIANTS_VARS}    username=jsmith    password=Dawan2021
@{LOGIN}          jsmith    Dawan2021

*** Test Cases ***
Dolibarr Connexion
    [Documentation]    Objectif tester la connexion à l'application GLPI
    Open Browser    ${urlDolibarr}    CHROME
    Input Text    id=username    ${LOGIN}[0]
    Input Text    name=password    ${IDENTIFIANTS_VARS}[password]
    Click Element    xpath=//input[contains(@value,'Login')]
    ${tableInfos}    Get WebElement    //table[.//td='Informations de connexion']
    Table Cell Should Contain    ${tableInfos}    2    1    Utilisateur
    Table Cell Should Contain    ${tableInfos}    2    2    SMITH
    Page Should Contain    Services à activer : 0
    Page Should Contain Element    //a[.='Accueil']
    Page Should Contain Image    //img[@alt='No photo']
    Close Browser
